def pyramid(height):
	if height >= 1 and height <= 2:
		drawPyramid(1)
	elif height >= 3 and height <= 4:
		drawPyramid(2)
	elif height >= 5 and height <= 6:
		drawPyramid(5)
	elif height >= 7:
		drawPyramid(6)

def drawPyramid(height):
	for x in xrange(1, height + 1):
		temp = ""
		
		for y in range(x):
			temp = temp + "1 "

		space = ""
		for z in range(height - x):
			space = space + " "
		temp = space + temp

		print temp


pyramid(6)