a = ["sheep", "fleece", "greece"]

def pallendrone(func):
	def checkPallendrone(a):
		return func(["".join([y for i,y in enumerate(x) if x[0] == x[-1]]) for x in a])
		
	return checkPallendrone


@pallendrone
def func1(a):
	return [''.join([y for i,y in enumerate(x) if x[i-1:i] != y]) for x in a]


print(func1(["sheep", "fleece", "greece", "ppappappapp", "dog", "peep", "bob"]))