# output 1, 9 , 25, 49, 81, 121
a = [pow(x, 2) for x in range(1,12) if x%2 != 0] 
print(a)

# output ['aa', 'ac' ....]
b = [str(chr(x)) + chr(y)  for x in xrange(97,123, 1) for y in xrange(x+1,123, 2)]
print(b)