class PurchaseHistoryDB:
	def __init__(self, conn):
		self.conn = conn

	def save_history(self, item_list):
		for index, value in enumerate(item_list):
			if value['qty'] != "0":
				self.conn.insert({"name" : value['name'], "price" : value['price'], "qty" : value['qty']})

	def get_history(self):
		return self.conn.find().sort("_id", -1)
