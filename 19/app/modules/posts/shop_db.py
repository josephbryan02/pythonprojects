from bson.objectid import ObjectId

class ShopDB:

	def __init__(self, conn):
		self.conn = conn

	def get_items(self):
		return self.conn.find()

	def save_inventory(self, name, price):
		self.conn.insert({"name" : name, "price" : price})

	def update_inventory(self, id, name, price):
		self.conn.update({"_id" : ObjectId(id)}, {"$set": {"name" : name, "price" : price}})