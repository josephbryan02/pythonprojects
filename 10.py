import random

def generate_fibonacci(n):
	temp = list()

	for x in xrange(n + 1):
		temp.append(x + 1)
		if len(temp) > 0 and len(temp) <= 2:
			temp[x] = 1
		else:
			temp[x] = temp[x-2] + temp[x - 1]

		yield temp[x]

for x in generate_fibonacci(6):
	print x
