var ItemList = React.createClass({
	render: function() {
		return(
			<li>{this.props.item_name} +'    ' +{this.props.price}</li>
		);
	}
});

var InventoryList = React.createClass({
	render: function() {
	    var postNodes = this.props.data.map(function (item, index) {
	      return (
	        <ItemList price={item.price} item_name={item.name}>
	          {item.name}
	        </ItemList>
	      );
	    });
	    return (
	      <div>
	        {{postNodes}}
	      </div>
	    );
	 }
});

var PostForm = React.createClass({
	handleSubmit: function() {
		e.preventDefault();
	    var form = e.target;
	    var item_name = form.item_name.value.trim();
	    var price = form.price.value.trim();
	    if (!price || !item_name) {
	      return;
	    }
	    this.props.onCommentSubmit({item_name: item_name, price: price});
	    form.item_name.value = '';
	    form.price.value = '';
	    return;
	},
	render: function() {
		return(
			<form action="get_inventory" method="post">
		        <input type="text" placeholder="Your name" name="item_name"/>
		        <input type="text" placeholder="Say something..." name="price"/>
		        <input type="submit" value="Post" />
		     </form>
		);
	}
})

var PostBox = React.createClass({
    getInitialState: function() {
    	return {data: []};
    },
    loadCommentsFromServer: function() {
    	$.ajax({
    		url : this.props.url,
    		type: "GET"
    	}).success(function(response) {
    		var postDict = JSON.parse(JSON.stringify(response));   
        	this.setState({data: postDict['posts']});
    	}.bind(this)).error(function(xhr, status, err) {
    		console.error(this.props.url, status, err.toString());
    	}.bind(this));
    },
    handleCommentSubmit: function(comment) {
    	$.ajax({
    		url  : this.props.url,
    		type : "POST",
    		dataType: 'json',
    		data : comment
    	}).success(function(response) {
    		var postDict = JSON.parse(JSON.stringify(response));   
        	this.setState({data: postDict['posts']});
    	}.bind(this))
    	.error(function(xhr, status, err) {
    		console.error(this.props.url, status, err.toString());
    	}.bind(this));
    },
    componentDidMount: function() {
    	this.loadCommentsFromServer();
    	setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function() {
      return(
      	<div>
      		<InventoryList data = {this.state.data} />
      		<PostForm onCommentSubmit={ this.handleCommentSubmit }/>
      	</div>
      );
    }
});


