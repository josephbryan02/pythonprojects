from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
import math
import random
import string
from flask import jsonify

shop = Blueprint("shop_cart", __name__)

@shop.route('/')
def shop_list():
	items = g.items.get_items()
	cart = g.cart.get_cart()
	history = g.history.get_history()

	cart_list = list()
	for index, value in enumerate(cart):
		_cart = {}
		_cart['name'] 		= value['name']
		_cart['price']		= value['price']
		_cart['qty']		= value['qty']
		_cart['total'] 		= str(int(value['price']) * int(int(value['qty'])))
		cart_list.append(_cart)
	
	return render_template("shop/index.html", items = items, cart = cart_list, history = history)


@shop.route('/get_inventory', methods=['POST', 'GET'])
def get_inventory():

	if request.method == "POST":
		item_name = request.form['item_name']
		price = request.form['price']
		g.items.save_inventory(item_name, price)
		return redirect(url_for('.shop_list'))

	items = g.items.get_items()

	inventory = list()
	for index, item in enumerate(items):
		temp 			= {}
		temp['name']	= item['name']
		temp['price'] 	= item['price']
		inventory.append(temp)

	return jsonify(posts=inventory)

@shop.route('/add_inventory', methods = ['POST'])
def add_inventory():
	item_name		= request.form['item_name']
	price		= request.form['item_price']
	g.items.save_inventory(item_name, price)

	return redirect(url_for('.shop_list'))

@shop.route('/add_to_cart', methods = ['POST'])
def add_to_cart():
	items 		= request.form.getlist('item_name[]')
	qty 		= request.form.getlist('item_qty[]')
	price 		= request.form.getlist('item_price[]')

	for index, item_name in enumerate(items):
		if qty[index] > 0:
			tag = g.cart.check_cart(item_name)
			if tag != 0:
				g.cart.update_cart(item_name, str(int(qty[index]) + int(tag[0]['qty'])))
			else:
				g.cart.add_cart(item_name, price[index], qty[index])

	return redirect(url_for('.shop_list'))

@shop.route('/purchase_item')
def purchase_item():
    cart = g.cart.get_cart()
    g.history.save_history(cart)
    g.cart.remove_all_item()

    return redirect(url_for('.shop_list'))

@shop.route('/update_item/', methods = ['GET'])
def update_item():
	old_name	= request.args['old_name']
	name 		= request.args['name']
	price 		= request.args['price']
	item_id 	= request.args['id']

	g.items.update_inventory(item_id, name, price)
	g.cart.update_item_price(old_name, name, price)

	return redirect(url_for('.shop_list'))








