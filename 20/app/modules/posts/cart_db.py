from bson.objectid import ObjectId

class CartDB:
	
	def __init__(self, conn):
		self.conn = conn

	def get_cart(self):
		return self.conn.find()
		
	def add_cart(self, name, price, qty):
		self.conn.insert({"name" : name, "price" : price, "qty" : qty})

	def check_cart(self, name):
		if self.conn.find({"name" : name}).count() > 0:
			return self.conn.find({"name" : name})
		else:
			return 0
	
	def remove_all_item(self):
		self.conn.remove()

	def update_cart(self, name, qty):
		self.conn.update({"name" : name}, {"$set": {"qty" : qty}})

	def update_item_price(self, old_name, name, price):
		self.conn.update({"name" : old_name}, {"$set" : {"name" : name, 'price' : price}})